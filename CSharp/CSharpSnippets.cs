private bool ValidDateAmount(decimal amount)
{
    var pattern = @"^\-?[0-9]+(?:\.[0-9]{1,2})?$";
    return Regex.IsMatch(amount.ToString(CultureInfo.InvariantCulture), pattern);
}


string emptyString = string.Empty;
//or 
string emptyString = "";


decimal convertToNegative = Math.Abs(1) * Decimal.MinusOne;
//or 
decimal convertToNegative = Math.Abs(1) * -1;


DateTime? date = GetDate();
if (date is null) {/*Do something...Cry???*/}                
//or 
if (date == null) {/*Do something...Cry???*/}  

