# Hunt down large files
sudo find / -type f -exec du -Sh {} + | sort -rh | head -n 5

# Clean out pihole
pihole flush

# Nuke the pihole logs
sudo rm pihole.log.*.*

# Restart pihole outside Docker image
# Function not supported in Docker images
pihole –up
