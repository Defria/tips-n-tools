# Usage:
<#
    Import-Module $PSScriptRoot\_ReleaseScriptHelpers\Modules\Logging\ExceptionHandler.psm1  -DisableNameChecking
    Import-Module $PSScriptRoot\_ReleaseScriptHelpers\Modules\Logging\Logger.psm1  -DisableNameChecking

    try {
            
        # DoWork -ErrorAction Stop
    }
    catch {
        Resolve-Error $_
        exit -1
    }
#>

function Resolve-Error($errorRecord) {
    $errorRecord | Format-List * -Force
    $errorRecord.InvocationInfo | Format-List *
    $Exception = $errorRecord.Exception
    for ($i = 0; $Exception; $i++, ($Exception = $Exception.InnerException)) {
        "$i" * 80
        $Exception | Format-List * -Force
    }
}
