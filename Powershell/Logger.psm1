# Usage:
<#
    Import-Module $PSScriptRoot\_ReleaseScriptHelpers\Modules\Logging\ExceptionHandler.psm1  -DisableNameChecking
    Import-Module $PSScriptRoot\_ReleaseScriptHelpers\Modules\Logging\Logger.psm1  -DisableNameChecking
    
    Write-LogMessage -Level INFO -Message "Foobar this a test info log message"
#>

function Write-LogMessage {
    Param(
    [Parameter(Mandatory=$False)]
    [ValidateSet("INFO","WARN","ERROR","FATAL","DEBUG")]
    [String]
    $Level = "INFO",

    [Parameter(Mandatory=$True)]
    [string]
    $Message
    )

    $Stamp = (Get-Date).toString("yyyy/MM/dd HH:mm:ss")
    $Line = "$Stamp $Level $Message"

    switch ($Level) {
        "INFO" { Write-Host $Line }
        "WARN" { Write-Host $Line -ForegroundColor Yellow}
        "ERROR" { Write-Error $Line}
        "FATAL" { Write-Error $Line}
        "DEBUG" { Write-Host $Line }
        Default {Write-Output $Line}
    }
}
